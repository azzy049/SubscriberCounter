/*


Author : Shaik Azhar Ahmed
Date 5th Feb, 2023

Please consider subsribing to the channel if this code helps
*/


#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <TM1637Display.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#define CLK 5
#define DIO 4

#include <YoutubeApi.h>
#include <ArduinoJson.h>


const uint8_t SEG_CONN[] = {
  SEG_G | SEG_D | SEG_E,                          // c
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F,  // O
  SEG_C | SEG_E | SEG_G,                          // n
  SEG_C | SEG_E | SEG_G,                          // n
};



const uint8_t SEG_SUB[] = {
  SEG_A | SEG_D | SEG_C | SEG_D,          // S
  SEG_E | SEG_D | SEG_C,                  // u
  SEG_F | SEG_E | SEG_G | SEG_C | SEG_D,  // b
  SEG_G,                                  // -
};

const uint8_t SEG_VIEWS[] = {
  SEG_F | SEG_B | SEG_D,          // V
  SEG_F | SEG_E,                  // u
  SEG_F | SEG_G | SEG_B | SEG_D,  // b
  SEG_A | SEG_F | SEG_C | SEG_D,  // -
};

const uint8_t SEG_VCount[] = {
  SEG_F | SEG_B | SEG_D,  // V
  SEG_D,
  SEG_E | SEG_G | SEG_D,  // c
  SEG_C | SEG_E | SEG_G   // n
};

const uint8_t SEG_OK[] = {
  SEG_D,                                  // S
  SEG_E | SEG_D | SEG_C | SEG_G,          // o
  SEG_F | SEG_E | SEG_G | SEG_C | SEG_A,  // k
  SEG_D,                                  // -
};

TM1637Display display(CLK, DIO);


//------- Replace the following! ------
char ssid[] = "";                          // your network SSID (name)
char password[] = "";                          // your network key
#define API_KEY ""  // your google apps API Token
#define CHANNEL_ID ""              // makes up the url of channel
//------- ---------------------- ------

WiFiClientSecure client;
YoutubeApi api(API_KEY, client);

unsigned long timeBetweenRequests = 6000;
unsigned long nextRunTime;

long subs = 0;
int switch_pin = 14; //GPIO5
int mode = 0;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 19820);


void setup() {

  Serial.begin(115200);
  display.setBrightness(0x0f);
  display.setSegments(SEG_CONN);
  // Set WiFi to station mode and disconnect from an AP if it was Previously
  // connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  pinMode(switch_pin, INPUT_PULLUP);
//  attachInterrupt(digitalPinToInterrupt(switch_pin), handleInterrupt, CHANGE);
  delay(100);


  // Attempt to connect to Wifi network:
  Serial.print("Connecting Wifi: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  display.setSegments(SEG_OK);
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  IPAddress ip = WiFi.localIP();
  Serial.println(ip);

  // Required if you are using ESP8266 V2.5 or above
  client.setInsecure();

  // If you want to enable some extra debugging
  api._debug = false;
  timeClient.begin();
}



void loop() {

  if (api.getChannelStatistics(CHANNEL_ID) && digitalRead(switch_pin)) {

    Serial.println( mode);
    Serial.println("------------------------");
    display.setSegments(SEG_SUB);
    delay(1000);
    display.clear();
    display.showNumberDec(api.channelStats.subscriberCount, false);  // Expect: ___1
    delay(8000);
    display.clear();
    display.setSegments(SEG_VIEWS);
    delay(1000);
    display.clear();
    display.showNumberDec(api.channelStats.viewCount, false);  // Expect: ___1
    delay(2000);
    display.clear();
    display.setSegments(SEG_VCount);
    delay(1000);
    display.clear();
    display.showNumberDec(api.channelStats.videoCount, false);  // Expect: ___1
    delay(1000);
  }
  else{
    showTime();
  }
}

void showTime(){
    timeClient.update();

  Serial.println(timeClient.getFormattedTime());
  int A,B;

  timeClient.update();
  A = timeClient.getHours() * 100 + timeClient.getMinutes();
  B = timeClient.getSeconds();
  if((B % 2) == 0)
  {
    display.showNumberDecEx(A, 0b01000000 , false, 4, 0); 
  }
  else
  {
    display.showNumberDecEx(A, 0b00000000 , false, 4, 0); 
  }

  delay(200);
}
